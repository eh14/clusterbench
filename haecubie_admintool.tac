#
# twistd application file used for REAL deployment. Note that starting the program with THIS file
# might shutdown your computer when requested by the server-part.
#

import os, sys, pythonioc

sys.path.append(os.path.abspath('src'))

from cb.node import admintool

config = pythonioc.Inject('config')

config.adminFakeShutdown=False

application = admintool.createApplication(config.adminPort)

