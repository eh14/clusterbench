import os, sys, pythonioc

sys.path.append(os.path.abspath('src'))

from cb import node

config = pythonioc.Inject('config')
config.name = 'node1'


config.workingDirectory='/tmp/clusterbench/node/'
application = node.createApplication()

