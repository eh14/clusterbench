import os, sys

sys.path.append(os.path.abspath('src'))

from cb import server

application = server.createApplication(8800, 8801, 8802)