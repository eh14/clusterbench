'''
Created on May 12, 2014

@author: eichhorn
'''

workingDir = '/home/linaro/store/haec/clusterbench'
PID_FILE = '/tmp/admintool.pid'
execute = ['sudo', 'twistd', '-ny', 'haecubie_admintool.tac', '--pidfile='+PID_FILE]

def shutdown(pid):
    import subprocess
    with open(PID_FILE, 'r') as pid:
        subprocess.Popen(['sudo', 'kill', pid.read().strip()])

repeat = True
