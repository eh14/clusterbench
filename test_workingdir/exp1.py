'''

@author: eh14
'''
from cb.experiment import Experiment
from twisted.internet import defer

class TestExperiment(Experiment):
    
    name = 'Experiment-1'
    
    @defer.inlineCallbacks
    def run(self):
        print "Phase1 - Experiment"

        # start recording performance counters with dstat
                
        # stop everything
        yield self.do('log', 'stop all running tools and wait')
        yield self.do('stopAll')
        yield self.wait(60)
        
        yield self.do('startTool', 'recorder')
        yield self.do('startTool', 'frequency')
        yield self.do('startTool', 'temp')
        
        yield self.do('collectTimeOffset')
        yield self.do('startPowerRecorder')
        
        yield self.do('log', 'record 1 hour in idle')
        yield self.wait(3600)
        
        # start stress        
        yield self.do('startTool', 'stress') 
        
        yield self.do('log', 'log 1 hour under stress')
        yield self.wait(3600)
        
        # stop stress
        yield self.do('stopTool', 'stress')
        
        yield self.wait(60)
        
        # stop everything
        yield self.do('stopAll')
        yield self.wait(60)
        
        self.do('log', 'Collecting the results...')

        yield self.do('retrieveFiles')
        self.do('log', 'All done.')
        
experiment = TestExperiment()
