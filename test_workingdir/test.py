'''

@author: eh14
'''
from cb.experiment import Experiment
from twisted.internet import defer

class TestExperiment(Experiment):
    
    name = 'Test'
    
    @defer.inlineCallbacks
    def run(self):
        print "running experiment"

        # start recording performance counters with dstat        
        yield self.do('startTool', 'recorder')
        
        yield self.do('collectTimeOffset')
        
        # start recording power consumption
        yield self.do('startPowerRecorder')
        
        # start recording frequency
        yield self.do('startTool', 'frequency')
        
        yield self.wait(2)
        
        # rset the frequency
        yield self.do('startTool', 'frequency', 'reset')
        
        # increment five times
        for i in range(5):
            yield self.wait(1)
            self.do('log', 'Increasing Frequency (%d)' % i)
            yield self.do('startTool', 'frequency', 'next')
        
        yield self.wait(1)
        
        # go back to ondemand
        yield self.do('startTool', 'frequency', 'ondemand')
        
        # stop all recorder-tools
        yield self.do('stopAll')
        yield self.wait(1)
        
        # collect all the files
        yield self.do('retrieveFiles')
        print "files retrieved"
experiment = TestExperiment()
