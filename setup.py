import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as f:
        return f.read()

setup(
    name="clusterbench",
    version="0.0.1",
    author="Franz Eichhorn",
    author_email="franz.eichhorn@tu-dresen.de",
    description=("Simple Cluster benchmarking tool"),
    license="BSD",
    keywords="example documentation tutorial",
    url="https://bitbucket.org/eh14/clusterbench",
    packages=['src'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: MIT License",
    ],
    install_requires=['twisted',
                      'pythonioc',
                      'pyzmq'
                      ],
      
    dependency_links=['https://bitbucket.org/eh14/twisted-utils/get/master.zip#egg=twisted-utils']
)
