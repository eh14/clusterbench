import os, sys, pythonioc

sys.path.append(os.path.abspath('src'))

from cb.node import admintool

config = pythonioc.Inject('config')

application = admintool.createApplication(config.adminPort)

