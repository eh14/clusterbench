from fabric.api import env
from fabric.decorators import roles as fabric_roles
from functools import wraps
import sys
import paramiko

from compiler.ast import flatten

def init_roles_from_hostfile():
    """ 
    sets the environment roles (basically role 'nodes') 
    by reading the /etc/hosts file and extracting all hosts
    """
    
    nodeList = []
    with open('/etc/hosts', 'r') as hostsFile:
        for line in hostsFile:
            if len(line.strip()) == 0:
                continue
            
            entries = line.split()

            if len(entries) != 2:
                print "Ignoring invalid line in /etc/hosts", line
                continue
            name = entries[1].strip()
            if name.startswith('slave'):
                nodeList.append(name.strip())
    
    nodeList.sort()
    
    if len(nodeList) == 0:
        sys.stderr.write("Did not find ANY nodes. You may want to update the hosts file first `sudo fab updateHosts`\n")
        
    # finally set it
    env.roledefs['nodes'] = nodeList 
    
def init_default_roles():
    env.roledefs['server'] = ['localhost']
    env.roledefs['all'] = flatten([env.roledefs['nodes'], env.roledefs['server']])


def isHostOnline(host):
    """
    Helper function to check whether a host is online on a low level with paramiko,
    """
    
    try:
        paramiko.Transport((host, int(env.port)))
        return True
    except Exception as e:
        return False

def safe_roles(*roleNames):
    """
    Replacement decorator that checks whether the host currently executed is 
    actually online (can be disabled by calling fab with --set NoAliveCheck=1)
    """
    
    # function that is being called by the original
    # roles decorator
    def __inner(func):
        
        @wraps(func)
        def saveFunction(*args, **kwargs):
            if isHostOnline(env.host) or hasattr(env, 'NoAliveCheck'):
                return func(*args, **kwargs)
            else:
                print "Host %s seems offline, ignoring task" % str(env.host)
    
        # call the original decorator with the names and the save function
        return fabric_roles(*roleNames)(saveFunction)
    return __inner
   