

from fabric.api import task, local, env, roles, parallel, run, put
from fabric.context_managers import lcd
import os
import sys
from fabric.tasks import execute
import subprocess
import time

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

MASTER_PID = '/tmp/master.pid'
NODE_PID = '/tmp/node.pid'

@task
def bootstrap():
    local('pip install nose')
    local('python setup.py develop')

@task
def server():
    local('twistd -ny server.tac --pidfile=%s' % MASTER_PID)

def kill(pidfile):
    try:
        with open(pidfile) as pid:    
            local('kill -INT %s' % (pid.read()))
    except Exception as e:
        print "Error killling the process", e

@task
def node():
    local('twistd -ny node.tac --pidfile=%s' % NODE_PID)

@task
def cambrionixMock(*slaves):
    if len(slaves) == 0:
        slaves = ['node1']
    local('PYTHONPATH=$PYTHONPATH:src && cd src && python cb/unittests/cambrionixmock.py ' + ' '.join(slaves))

@task
def check(test=''):
    testToRun = 'cb.unittests%s' % ('.' + test if len(test) > 0 else '')
    
    local('PYTHONPATH=$PYTHONPATH:src && cd src && trial --temp-directory=/tmp/_trial_tmp ' + testToRun)


@task
def client(experiment=''):
    with lcd('src'):
        if experiment:
            experiment = '--experiment=' + experiment
        local('PYTHONPATH=.:$PYTHONPATH python cb/client.py --working-directory=%s %s' % (os.path.abspath('test_workingdir'), experiment))

@task
def admintool():
    local('sudo twistd -y admintool.tac --pidfile=/tmp/admintool.pid')
    
@task
def killAdminTool():
    result = local('sudo cat /tmp/admintool.pid', capture=True)
    local('sudo kill %s' % result.stdout)
        
@task
def dev(experiment='', showOutput=False):
    server = None
    node = None
    
    null = open(os.path.devnull, 'w')
    err = out = null
    
    if showOutput:
        out = sys.out
        err = sys.err
    try:
        print "starting admin tool"
        execute(admintool)
        print "starting server"
        server = subprocess.Popen(['twistd', '-ny', 'server.tac', '--pidfile=%s' % MASTER_PID], stdout=out, stderr=err)
        
        print "starting node"
        node = subprocess.Popen(['twistd', '-ny', 'node.tac', '--pidfile=%s' % NODE_PID], stdout=out, stderr=err)
        
        print "starting client"
        time.sleep(0.5)
        execute(client, experiment)
    finally:
        if server:
            print "Killing the servers"
            server.terminate()
        
        if node:
            print "killing node"
            node.terminate()

        execute(killAdminTool)


import cubiedeploy

@task
def deploy():
    cubiedeploy.init_roles_from_hostfile()
    cubiedeploy.init_default_roles()
    
    env.use_ssh_config = True
    env.user = 'linaro'
    env.password = 'linaro'
    
    env.disable_known_hosts = True
    env.deploy_dir = '/home/linaro/store/haec/clusterbench'
    
    local('git pull')

    execute(deployServer)
    execute(deployNode)
    
@task
@cubiedeploy.safe_roles('nodes')
@parallel
def deployNode():
    print "deploying to node", env.host_string
    run('mkdir -p ' + env.deploy_dir)
    local('rsync -a --exclude ".*/" --no-links ./ %s:%s' % (env.host_string, env.deploy_dir))
    put('startadmintool.py', '/home/linaro/store/haec/start/10_cb_admintool.py')
    put('startnode.py', '/home/linaro/store/haec/start/10_cb_node.py')
    
@task
@cubiedeploy.safe_roles('server')
def deployServer():
    print "deploying server"
    run('mkdir -p ' + env.deploy_dir)
    local('rsync -a --exclude ".*/" --no-links ./ %s' % env.deploy_dir)
    put('startserver.py', '/home/linaro/store/haec/start/10_cb_server.py')
