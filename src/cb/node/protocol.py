from twisted.spread import pb
import pythonioc
from twisted.internet import defer, task, reactor
from twisted.python import log
from cb.tools import filetransfer
from cb.node import admintool
import time
import shlex
import subprocess

class NodeSession(pb.Referenceable):
    config = pythonioc.Inject('config')
    experimentService = pythonioc.Inject('experimentService')
    def __init__(self, name):
        self.name = name
        
    def remote_name(self):
        return self.config.name
    
    @defer.inlineCallbacks
    def remote_prepareEnvironment(self):
        yield self.experimentService.prepareEnvironment()
        
    @defer.inlineCallbacks
    def remote_experimentStatus(self):
        status = yield  self.experimentService.getExperimentStatus()
        defer.returnValue(status)
        
    @defer.inlineCallbacks
    def remote_prepareExperiment(self, name):
        yield self.experimentService.prepareExperiment(name)
        
    @defer.inlineCallbacks
    def remote_startTool(self, tool, *args, **kwargs):
        yield self.experimentService.startTool(tool, *args, **kwargs)
        
    @defer.inlineCallbacks
    def remote_stopTool(self, tool):
        yield self.experimentService.stopTool(tool)
        
    def remote_getToolList(self):
        return self.experimentService.listTools()
    
    @defer.inlineCallbacks
    def remote_getAllToolStatus(self):
        result = yield self.experimentService.getAllToolStatus()
        
        defer.returnValue(result)
        
    @defer.inlineCallbacks
    def remote_stopAll(self):
        yield self.experimentService.stopAll()
        
    @defer.inlineCallbacks
    def remote_sendAllFiles(self, target):
        yield self.experimentService.sendAllFiles(target)
        
    @defer.inlineCallbacks
    def remote_shutdown(self):
        admin = yield admintool.connectToAdmin()
        yield admin.callRemote('shutdown')
        
    def remote_getLocalTime(self):
        return time.time()
    
    @defer.inlineCallbacks
    def remote_executeCommand(self, commandLine):
        cmd = subprocess.Popen(shlex.split(commandLine), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        end = time.time() + 60
        
        while time.time() < end:
            # wait 500ms
            yield task.deferLater(reactor, 0.25, lambda:None)
            
            if cmd.poll() is not None:
                (stdout, _) = cmd.communicate()
                defer.returnValue(stdout)
            
        else:
            cmd.kill()
            defer.returnValue('Command timeout')
        
    
class Node(pb.Referenceable):

    def __init__(self, name):
        self.name = name
        
    def remote_createSession(self):
        return NodeSession(self.name)
    
    def remote_name(self):
        return self.name 
    
class NodeServer(pb.Root):
    """
    Server which nodes connect to.
    """
    nodeRegistry = pythonioc.Inject('nodeRegistry')    
    def remote_connect(self, node):
        log.msg("Node " + str(node) + " connected")
        self.nodeRegistry.addNode(node)


