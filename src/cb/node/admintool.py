'''

@author: eh14
'''
import pythonioc
from twisted.internet import defer, reactor
from twisted.spread import pb
from twisted.python import log
from twutils import timeprovider

from cb.tools import utils
from cb import config
from twisted.application import internet, service

pythonioc.registerService(timeprovider.TimeProvider)
pythonioc.registerService(config.Config)


config = pythonioc.Inject('config')


class Admin(pb.Root, utils.CommandLineCallerMixin):
    
    @defer.inlineCallbacks
    def remote_cpufreqSet(self, command):
        cmdList = command.split(' ') if isinstance(command, str) else command
        cmd = ['cpufreq-set'] + cmdList
        log.msg('Executing %s' % (' '.join(cmd)))
        yield self._getProgramOutput(cmd)
        
    def remote_shutdown(self):
        if config.adminFakeShutdown:
            print "***Would shutdown***"
            return
        
        # do the real shutdown
        self._getProgramOutput(['shutdown', '-h', '0'])
        
        
            
def createApplication(port):
    
    application = service.Application('admin')
    
    factory = pb.PBServerFactory(Admin())
    srv = internet.TCPServer(port, factory)
    srv.setServiceParent(application)
    
    return application
        
    
    
@defer.inlineCallbacks
def connectToAdmin():
    factory = pb.PBClientFactory()
    reactor.connectTCP('localhost', config.adminPort, factory)
    admin = yield factory.getRootObject()
    
    defer.returnValue(admin)
    