'''

@author: eh14
'''

from twisted.spread import pb
from twisted.internet import reactor, defer
import protocol
from twisted.application.service import Application
from twisted.application import internet
from twisted.internet.error import ConnectionRefusedError
from cb.tools import recorder, frequency
from twutils import timeprovider
import pythonioc

from cb import config
from cb.node import experiment
from twisted.internet.protocol import ReconnectingClientFactory

pythonioc.registerService(timeprovider.TimeProvider)
pythonioc.registerService(config.Config)
pythonioc.registerService(experiment.ExperimentService)
pythonioc.registerService(frequency.FrequencyService)


__services = None

class ReconnectingPBClientFactory(pb.PBClientFactory):

    def clientConnectionFailed(self, connector, reason):
        print "connection failed"
        reactor.callLater(1, connector.connect)
    def clientConnectionLost(self, connector, reason):
        print "connection lost", connector, reason
        pb.PBClientFactory.clientConnectionLost(self, connector, reason, reconnecting=1)
        reactor.callLater(1, connector.connect)

factory = ReconnectingPBClientFactory()

@defer.inlineCallbacks
def connect():
    config = pythonioc.Inject('config')
    
    root = yield factory.getRootObject()
    root.callRemote('connect', protocol.Node(config.name))
    
    # when the root is gone, try to get it again later
    root.notifyOnDisconnect(lambda _x: reactor.callLater(1, connect))
    
def createApplication():
    global __services
    experimentService = pythonioc.Inject('experimentService')
    
    reactor.callLater(0, connect)
    
    application = Application("node")
    
    config = pythonioc.Inject('config')
    
    nodeService = internet.TCPClient(config.serverHost, config.serverPort, factory)
    nodeService.setServiceParent(application)
    __services = nodeService
    
    reactor.addSystemEventTrigger("before", "shutdown", experimentService.stopAll)
    
    return application


if __name__ == '__main__':
    import sys
    from twisted.python import log
    log.startLogging(sys.stdout)
    
    createApplication()
    __services.startService()
    reactor.run()
