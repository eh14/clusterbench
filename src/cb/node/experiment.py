'''

@author: eh14
'''
from twisted.internet import defer
import pythonioc
import os
from twisted.python import log

from cb.tools import filetransfer, recorder, frequency, stress, temp
import collections

class ExperimentService(object):


    CHUNK_SIZE = 1024
    SEND_INTERVAL = 0.001
    config = pythonioc.Inject('config')
    
    def __init__(self): 
        self._tools = collections.OrderedDict([
                                               ('recorder', recorder.RecorderTool()),
                                               ('frequency', frequency.FrequencyTool()),
                                               ('stress', stress.Stress()),
                                               ('temp', temp.TemperatureTool())
                                               ])
    def postInit(self):
        self.prepareEnvironment()

    def prepareEnvironment(self):
        if not os.path.exists(self.config.workingDirectory):
            os.makedirs(self.config.workingDirectory)
     
    def listTools(self):
        return self._tools.keys()

    def _checkToolExists(self, tool):    
        if tool not in self._tools:
            raise AttributeError('Invalid Tool %s' + tool)
        
    @defer.inlineCallbacks
    def startTool(self, tool, *args, **kwargs):
        self._checkToolExists(tool)
        yield self._tools[tool].start(*args, **kwargs)
         
    @defer.inlineCallbacks
    def stopTool(self, tool):
        self._checkToolExists(tool)
        yield self._tools[tool].stop()
        
    def stopAll(self):
        log.msg('Stopping All Tools')
        for (toolName, tool) in self._tools.iteritems():
            log.msg('Stopping %s' % toolName)
            tool.stop()
        
    @defer.inlineCallbacks
    def getToolStatus(self, tool):
        self._checkToolExists(tool)
        res = yield self._tools[tool].getStatus()
        defer.returnValue(res)
        
    @defer.inlineCallbacks
    def getAllToolStatus(self):
        # request the status of all tools
        dl = [defer.maybeDeferred(tool.getStatus) 
              for tool in self._tools.itervalues()]
        
        # wait for them
        results = yield defer.DeferredList(dl)
        
        # return only the results
        defer.returnValue({tool:d[1] for tool, d in zip(self._tools.iterkeys(), results)})  
        
    @defer.inlineCallbacks
    def getExperimentStatus(self):
        toolStats = yield self.getAllToolStatus()
        
        defer.returnValue({
                # 'state':self._experimentState,
                'tools':toolStats
                })
        

    @defer.inlineCallbacks
    def sendAllFiles(self, target):
        # send recorder file
        for tool in self._tools.itervalues():
            outputFile = tool.getOutputFile()
            if os.path.exists(outputFile):
                yield filetransfer.writeFile(outputFile,
                         target,
                         self.CHUNK_SIZE,
                         self.SEND_INTERVAL)
