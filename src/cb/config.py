'''

@author: eh14
'''
import os
import uuid



class Config(object):
    """
    Encapsulates the node configuration. Will be registered by the node.
    """
    workingDirectory = None
    name = 'node-%s' % (str(uuid.uuid4())[:8])
    
    adminPort = 14300
    cambrionixUrl = 'tcp://127.0.0.1:9059'
    
    # safety configuration option so the
    # admin does not shutdown any test machines.
    adminFakeShutdown = True
    
    maxShutdownTime = 30
    
    
    serverHost = 'localhost'
    serverPort = 8800
    def postInit(self):
        self.workingDirectory = os.getcwd()

    def getWdSubdir(self, *subdirs):
        """
        Returns subdirectories of the working directory
        """
        dirs = [self.workingDirectory] + list(subdirs)
        return os.path.join(*dirs)
    
