from twisted.internet import defer
import pythonioc
from twisted.spread import pb

class NotConnectedError(pb.Error):
    pass

class SessionManager(object):
    eventService = pythonioc.Inject('eventService')
    nodeRegistry = pythonioc.Inject('nodeRegistry')
    
    def __init__(self, clientOutput):
        self._out = clientOutput
        self._sessions = None
        
    @defer.inlineCallbacks
    def init(self):
        """
        Initializes the all sessions from the list of nodes by requesting
        new session objects.
        """
        self._sessions = {}
        self.eventService.subscribeEvent('node-connected', self._addNode)
        self.eventService.subscribeEvent('node-disconnected', self._removeNode)
        
        dl = []
        for node in self.nodeRegistry.nodes:
            dl.append(self._addNode(node))
        
        yield defer.DeferredList(dl)
        
    def iterNodes(self):
        return self._sessions.iteritems()
    
    def getAllNames(self):
        return self._sessions.keys()
    
    def isConnectedNode(self, nodeName):
        return nodeName in self._sessions
        
    def getSession(self, nodeName):
        return self._sessions[nodeName]
    
    def _removeNode(self, nodeName):
        self._out.send('Lost connection to node ' + nodeName)
        del self._sessions[nodeName]

    @defer.inlineCallbacks
    def _addNode(self, node):
        
        def _removeSession(name):
            self._out.send('Lost connection to node %s' % name)
            del self._sessions[name]
        
        @defer.inlineCallbacks
        def _addSession(session):
            try:
                print "adding session"
                name = yield session.callRemote('name')
                self._sessions[name] = session
                session.notifyOnDisconnect(lambda _: _removeSession(name))
                self._out.send('Successfully connected to node %s' % name)
            except Exception as e:
                print "Error connecting to node " + str(session)
        
        try:
            session = yield node.callRemote('createSession')
            _addSession(session)
        except Exception as e:
            self._out.send('Error creating node session %s' % e)
        
    def waitForBroadcast(self, broadcast):            
        """
        Convenience function creating a deferredList of the deferreds in the list, 
        returned by _broadcastToNodes.
        """
        
        return defer.DeferredList([d[1] for d in broadcast])
        
    def broadcastToNodes(self, method, *args, **kwargs):
        """
        Calls a remote function on all nodes.
        Returns a list of tuples (session, result)
        """
        
        if self._sessions is None:
            raise NotConnectedError()
         
        dl = []
        for sessionName, session in self._sessions.iteritems():
            dl.append((sessionName, session.callRemote(method, *args, **kwargs)))
            
        return dl
    
    @defer.inlineCallbacks
    def broadcastAndWait(self, method, *args, **kwargs):
        """
        Runs the method and the args on all nodes, returning
        a list of tuples (nodeName, result).
        """
        
        dl = self.broadcastToNodes(method, *args, **kwargs)
        
        nodes = [d[0] for d in dl]
        
        result = yield defer.DeferredList([d[1] for d in dl])
        
        defer.returnValue(zip(nodes, [r[1] for r in result]))
    
    def clientPrint(self, message):
        """
        Sends a message to the session client.
        """
        
        self._out.send(message)


