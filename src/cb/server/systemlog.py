'''

@author: eh14
'''
import pythonioc
import os



class SystemLog(object):
    
    LOG_NAME = 'system.log'
    
    timeProvider = pythonioc.Inject('timeProvider')
    
    # 
    # opened log file. Call :create: to open it. It will automatically be closed when the service is destroyed. 
    _log = None
    
    def create(self, targetFolder):
        if self._log:
            self.close()
            
        self._log = open(os.path.join(targetFolder, self.LOG_NAME), 'w')
    
    def preDestroy(self):
        self.close()
        
    def close(self):
        assert self._log
        
        self._log.close()
    
    def log(self, msg):        
        if not self._log:
            raise Exception('Log not opened. Did you call prepare_experiment?')
        
        
        print >> self._log, self.timeProvider.now(), msg
        self._log.flush()
    

    