'''

@author: eh14
'''

from twisted.spread import pb
from twisted.internet import defer, task
import pythonioc
import logging
from cb.server import session
from cb.tools import filetransfer
from twisted.python import log, failure
import os
import random

class NodeRegistry(object):
    
    eventService = pythonioc.Inject('eventService')
    
    def postInit(self):
        self.nodes = set()
     
    @defer.inlineCallbacks
    def addNode(self, node):
        self.nodes.add(node)
        nodeName = yield node.callRemote('name')
        log.msg('Node %s added to NodeRegistry' % nodeName)
        node.notifyOnDisconnect(lambda _x: self._remove(nodeName, node))
        self.eventService.triggerEvent('node-connected', node)
        
    def _remove(self, nodeName, node):
        log.msg("Node " + nodeName + " disconnected")
        self.eventService.triggerEvent('node-disconnected', nodeName)
        self.nodes.remove(node)
         
 
class ClientOutputWrapper(object):
    
    _output = None
    
    def setOutput(self, output):
        self._output = output
        
        # set it to None again, if the output is disconnected because
        # the client died.
        output.notifyOnDisconnect(lambda _:setattr(self, '_output', None))
       
    def send(self, what): 
        if not self._output:
            log.msg('Error sending output, the output reference is not set yet')
            log.msg('Message was %s' % what)
            return
        
        self._output.callRemote('out', str(what))
        
class ClientServer(pb.Root):
    """
    Server Root which clients connect to.
    """
    
    log = logging.getLogger('ClientServer')
    config = pythonioc.Inject('config')
    topologyService = pythonioc.Inject('topologyService')
    _sessions = None
    timeProvider = pythonioc.Inject('timeProvider')
    powerRecorder = pythonioc.Inject('powerRecorder')
    
    cambrionixService = pythonioc.Inject('cambrionixService')
    
    reactor = pythonioc.Inject('reactor')
    
    systemLog = pythonioc.Inject('systemLog')
    
    _currentExperimentFolder = None
    
    def __init__(self):
        self._output = ClientOutputWrapper()
        
    @defer.inlineCallbacks
    def remote_connect(self):
        self._sessions = session.SessionManager(self._output)
        yield self._sessions.init()
        
    def remote_setOutput(self, output):
        self._output.setOutput(output)

        
    @defer.inlineCallbacks
    def remote_prepareExperiment(self, name, stop=True):
        if stop is True:
            self._output.send('Stopping old experiment (if any)')
            yield self._sessions.broadcastAndWait('stopAll')
            
        yield self._sessions.broadcastAndWait('prepareEnvironment')
        self._currentExperimentFolder = self.config.getWdSubdir(name)
        
        self._output.send('Experiment folder is now ' + self._currentExperimentFolder)
        if not os.path.exists(self._currentExperimentFolder):
            self._output.send('Not existing yet, will create it...')
            os.makedirs(self._currentExperimentFolder)
            
        self.systemLog.create(self._currentExperimentFolder)
        self.systemLog.log('Experiment started')
        
        
    def remote_getCurrentExperimentFolder(self):
        return self._currentExperimentFolder
        
    @defer.inlineCallbacks
    def remote_startPowerRecorder(self):
        self._failIfNoExperiment()
        yield self.powerRecorder.start(os.path.join(self._currentExperimentFolder, 'power.log'))
        
        
    @defer.inlineCallbacks
    def remote_stopPowerRecorder(self):
        self._failIfNoExperiment()
        yield self.powerRecorder.stop()
        
    @defer.inlineCallbacks
    def remote_startTool(self, tool, *args, **kwargs):
        yield self._sessions.broadcastAndWait('startTool', tool, *args, **kwargs)
        
    @defer.inlineCallbacks
    def remote_stopTool(self, tool):
        yield self._sessions.broadcastAndWait('stopTool', tool)
        
    @defer.inlineCallbacks
    def remote_stopAll(self):
        yield self._sessions.broadcastAndWait('stopAll')
        yield self.remote_stopPowerRecorder()
        
    @defer.inlineCallbacks
    def remote_getToolStatus(self):
        res = yield self._sessions.broadcastAndWait('getAllToolStatus')
        for i, item in enumerate(res):
            if isinstance(item[1], failure.Failure):
                res[i] = (res[i][0], {'error':str(item[1])})
        res.append(('master', {'power':self.powerRecorder.getStatus()}))
        defer.returnValue(res)
        
    @defer.inlineCallbacks
    def remote_getToolList(self):
        # iterate over all sessions and try to get a tool-list
        for _, session in self._sessions.iterNodes():
            res = yield session.callRemote('getToolList')
            if res:
                defer.returnValue(res)
                
        else:
            defer.returnValue([])

    def _failIfNoExperiment(self):            
        if self._currentExperimentFolder is None:
            raise RuntimeError("no experiment folder set. Call prepareExperiment first!")
        
    @defer.inlineCallbacks
    def remote_retrieveFiles(self):
        self._failIfNoExperiment()
        dl = []
        for name, node in self._sessions.iterNodes():
            self._output.send('Requesting all files from node ' + name)
            target = filetransfer.TargetFile(os.path.join(self._currentExperimentFolder, name),
                                             self._output)
            d = node.callRemote('sendAllFiles', target)
            dl.append(d)
            
        yield defer.DeferredList(dl)
        
        print "sending files finished"
        
    @defer.inlineCallbacks    
    def remote_nodeStatus(self):
        
        dl = self._sessions.broadcastToNodes('experimentStatus')
        
        # add a printer for each node
        for d in dl:
            d[1].addCallback(lambda result:self._output.send('Node %s: %s' % (d[0], result)))

        yield self._sessions.waitForBroadcast(dl)

    @defer.inlineCallbacks
    def remote_clusterState(self):
        # if the power recorder is not running, request updates from the cambrionix
        if self.powerRecorder.getStatus() != self.powerRecorder.STATUS_RUNNING:
            yield self.cambrionixService.readCambrionixValues()

        # get the values from the topology-service and prepare for frontend        
        topo = yield self.topologyService.getAllNodes()
        
        states = []
        for itemName, item in topo.iteritems():
            state = {'name':itemName,
                     'power':item.status.status,
                     'online':self._sessions.isConnectedNode(itemName)}
            
            states.append(state)
            
        defer.returnValue(states)

    @defer.inlineCallbacks
    def remote_turnOnNodes(self, *nodeNames):
        @defer.inlineCallbacks
        def turnOn(node):
            try:
                yield self.topologyService.activateNode(node)
                self._output.send('Node %s activated' % node)
            except Exception as e:
                self._output.send('Error activating node %s: %s' % (node, (str(e))))
            
        yield self._applyActionToNodeList(nodeNames, turnOn)
            
            
    def remote_turnOffNodes(self, *nodeNames):
        
        @defer.inlineCallbacks
        def turnOff(node):
            try:
                self._output.send('Node %s shutdown requested' % node)
                yield self.topologyService.shutdownNode(self._sessions, node)
            except Exception as e:
                self._output.send('Error deactivating node %s: %s' % (node, (str(e))))
        
        self._applyActionToNodeList(nodeNames, turnOff)

    def _applyActionToNodeList(self, nodeNames, action):
        """
        applies a function to all nodes. Expand 'switches' to all switches 
        and 'nodes' or 'slaves' to all slaves.
        
        returns a DeferredList that fires when all actions have been finished.
        """
        
        allNodeNames = self.topologyService.getNodeNameList()
        
        dl = []
        for name in nodeNames:
            if name == 'switches':
                dl.extend([action(s) for s in allNodeNames if s.startswith('switch')])
            elif name in ['nodes', 'slaves']:
                dl.extend([action(s) for s in allNodeNames if s.startswith('slave')])
            else:
                dl.append(action(name))
        
        return defer.DeferredList(dl)

    @defer.inlineCallbacks
    def remote_executeCommand(self, commandLine):
        self._output.send('Executing %s on all nodes' % commandLine)
        self._output.send('To guarantee liveliness, the command will will be killed after 60s timeout')
        
        results = yield self._sessions.broadcastAndWait('executeCommand', commandLine)
        
        defer.returnValue(results)
        
    @defer.inlineCallbacks
    def remote_collectTimeOffset(self):
        
        rand = random.Random(self.timeProvider.now())
        
        @defer.inlineCallbacks
        def getOffsetForNode(name, node):
            
            sampleOffsets = []
            for i in range(10):
                send = self.timeProvider.now()
                nodeTime = yield node.callRemote('getLocalTime')
                receive = self.timeProvider.now()
                
                # get round-trip-time
                rtt = receive - send
                if(rtt > 0.25):
                    self._output.send('Round-Trip-Time for node {name} is too high ({rtt}). Offset calculation might not be accurate'.format(name=name, rtt=rtt))
                # calculate the offset to my time assuming send and 
                # receive took the same time (half round-trip)
                # it's like: my-time - its-time = offset
                sampleOffsets.append((send + float(rtt) / 2) - nodeTime)
                
                # wait for random number of seconds (0-100ms)
                yield task.deferLater(self.reactor, rand.random() * 0.1, lambda:None)
                
            
            defer.returnValue((name, float(sum(sampleOffsets) / len(sampleOffsets))))
        
        
        dl = []
        for name, node in self._sessions.iterNodes():
            dl.append(getOffsetForNode(name, node))

        results = yield defer.DeferredList(dl)

        # put results to dict, ignoring those which failed
        offsets = {r[1][0]:r[1][1] for r in results if r[0]}
        
        
        if any([not r[0] for r in results]):
            self._output.send("Calculating the time offsets failed at least for one node. This shouldn't happen")
        
        offsetFile = os.path.join(self._currentExperimentFolder, 'offsets')
        self._output.send('Writing offsets to file ' + offsetFile)
        with open(offsetFile, 'w') as out:
            print >> out, ' '.join(offsets.keys())
            print >> out, ' '.join(['%.6f' % value for value in offsets.itervalues()])
        
        defer.returnValue(offsets)
        
    def remote_log(self, message):
        """
        allows to send various messages to the system log. The log will be written using the current timestamp.
        """
        self.systemLog.log(message)
        
