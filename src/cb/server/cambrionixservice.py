'''
Created on Jun 17, 2014

@author: eh14
'''
import zmq
from twisted.python import log

from cb import config
from cb.tools import utils
import time
from twisted.internet import defer
import pythonioc
from twutils import eventservice


class CambrionixException(Exception):
    pass



class CambrionixConnector(object):
    _ctx = zmq.Context()
    cfg = pythonioc.Inject(config.Config)
        
    def _createSocket(self):
        sock = self._ctx.socket(zmq.REQ)  # @UndefinedVariable
        sock.connect(self.cfg.cambrionixUrl)
        
        return sock
    
    def getZmqContext(self):
        """
        returns the context used by the cambrionix. Mainly for unitt-testing
        """
        return self._ctx
    
    def _sendRequest(self, code, requireResponseCode=None, data=None):
        sock = None
        try:
            sock = self._createSocket()
            poller = zmq.Poller()
            sock.send_json({'code':code, 'data':data})
            poller.register(sock, zmq.POLLIN)  # @UndefinedVariable
            result = poller.poll(1000)
            if not result:
                raise CambrionixException('Timeout for code %s ' % code)
            else:
                data = sock.recv_json()
                if requireResponseCode and ('code' not in data or data['code'] != requireResponseCode):
                    raise CambrionixException('expected response code %s, got %s' % (requireResponseCode, data))
                else:
                    return data
        except CambrionixException:
            raise
        except Exception as e:
            raise CambrionixException(e)
        finally:
            if sock:
                sock.close(0)
                
    @utils.runAsDeferredThread
    def restrictProfiles(self):
        self._sendRequest('enable-profiles', requireResponseCode='ok', data=[1])
      
    @utils.runAsDeferredThread
    def getNodeState(self):
        """
        Retrieves the nodes state.
        """
        return self._sendRequest('nodes-state')
    @utils.runAsDeferredThread
    def turnOffNode(self, nodeName):
        self._sendRequest('node-deactivate', requireResponseCode='ok', data=nodeName)
    
    @utils.runAsDeferredThread
    def turnOnNode(self, nodeName):
        self._sendRequest('node-activate', requireResponseCode='ok', data=nodeName)

    @utils.runAsDeferredThread
    def getNetworkTopology(self):
        return self._sendRequest('network-topology', requireResponseCode='network-topology')['data']


class CambrionixService(object):
    
    _cambrionixConnector = pythonioc.Inject(CambrionixConnector)
    sensorstore = pythonioc.Inject('sensorStore')
    topologyService = pythonioc.Inject('topologyService')
    eventService = pythonioc.Inject(eventservice.EventService)
    
    @defer.inlineCallbacks
    def readCambrionixValues(self, ignoreErrors=True):
        try:
            nodeState = yield self._cambrionixConnector.getNodeState()
            self.topologyService.updateNodeStates(nodeState['data'])
            
            defer.returnValue(nodeState['data'])
            
        except CambrionixException as e:
            log.msg('WARNING: Exception reading cambrionix values. (%s)' % str(e))
            if not ignoreErrors:
                raise e
                
    @defer.inlineCallbacks
    def setupCambrionixTopology(self, ignoreErrors=True):
        topology = yield self._cambrionixConnector.getNetworkTopology()
        self.topologyService.initTopology(topology)
        
    @defer.inlineCallbacks
    def lateInit(self):
        """
        some time after the system runs, this method should be called. 
        It basically disables all profiles except one (issue #48)
        """
        log.msg('Enabling profile 1, disabling all others')
        yield self._cambrionixConnector.restrictProfiles()
    
