from twisted.application import service
from cb.server import protocol, cambrionixservice, topologyservice, session, \
    systemlog
from cb.node import protocol as nodeProtocol
from twisted.spread import pb
from twisted.internet import reactor
import pythonioc
from twutils import eventservice, timeprovider
from cb.tools import looper, power


class ClientService(service.Service):
    """
    Service that manages connections to clients.
    """    
    
    def __init__(self, port):
        self.port = port
        
    def startService(self):
        pbServerFactory = pb.PBServerFactory(protocol.ClientServer())
        self._port = reactor.listenTCP(self.port, pbServerFactory)
        
    def stopService(self):
        self._port.stopListening()
        
class NodeService(service.Service): 
    """
    Service that manages the connections with nodes.
    """
    
    def __init__(self, port):
        self.port = port
    
    def startService(self):
        self._pbServerFactory = pb.PBServerFactory(nodeProtocol.NodeServer())
        self._port = reactor.listenTCP(self.port, self._pbServerFactory)
        
    def stopService(self):
        self._port.stopListening()
        
        
cambrionixService = pythonioc.Inject('cambrionixService')

__services = None

def createApplication(nodeServerPort, clientServerPort, fileServerPort):
    
    global __services
    pythonioc.registerService(timeprovider.TimeProvider)
    pythonioc.registerService(eventservice.EventService)
    pythonioc.registerService(cambrionixservice.CambrionixConnector)
    pythonioc.registerService(cambrionixservice.CambrionixService)
    pythonioc.registerService(topologyservice.TopologyService)
    pythonioc.registerService(protocol.NodeRegistry)
    pythonioc.registerService(power.PowerRecorder)
    pythonioc.registerService(systemlog.SystemLog)
    pythonioc.registerServiceInstance(reactor, 'reactor')

    _reactor = pythonioc.Inject('reactor')
    #
    # start a job to set up the cambrionix topology.
    looper.startRetryingOneshotJob(_reactor,
        cambrionixService.setupCambrionixTopology,
        tryInterval=2,
        maxRetries=50)
    
    # 
    # start a job that will disable all profiles in cambrionix
    looper.startRetryingOneshotJob(_reactor, cambrionixService.lateInit, tryInterval=10, maxRetries=2)

    application = service.Application('ClusterBench')
    
    services = service.MultiService()
    
    __services = services

    ClientService(clientServerPort).setServiceParent(services)
    NodeService(nodeServerPort).setServiceParent(services)
    
    services.setServiceParent(application)
    
    _reactor.addSystemEventTrigger('before', 'shutdown', pythonioc.cleanServiceRegistry)  # @UndefinedVariable
    return application


if __name__ == '__main__':
    from twisted.python import log
    import sys
    log.startLogging(sys.stdout)
    application = createApplication(8800, 8801, 8802)
    __services.startService()
    reactor.run()