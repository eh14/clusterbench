'''
This module provides base classes for experiment execution.
'''
from twisted.internet import defer, task, reactor

class Experiment(object):
    server = None
    
    @defer.inlineCallbacks
    def do(self, method, *args, **kwargs):
        """
        Convenience method to call a method on the pb server session.
        """
        print "Executing server commands", method, ' '.join(args) 
        assert self.server, 'set server variable in experiment execution controller'
        result = yield self.server.callRemote(method, *args, **kwargs)
        # print only if there is a result, otherwise the 
        # console will be full of "None"s
        if result is not None:
            print result
            
            
         # if the method was not log, log what has been done
        if method != 'log':
            self.server.callRemote('log', '%s %s %s' % (method, ' '.join(args), 
                                                    ' '.join(['%s:%s' % (k, v) for k, v in kwargs])
                                                    )
                                   )
            
        defer.returnValue(result)
        
    def wait(self, seconds): 
        return task.deferLater(reactor, seconds, lambda:None)
    
    @defer.inlineCallbacks
    def prepare(self):
        name = self.name if hasattr(self, 'name') else self.__class__.__name__
        print "-----------Experiment **%s**-------------" % name
        print "Preparing Experiment"
        yield self.do('prepareExperiment', name)
            
    def run(self):
        """
        Implement this method to run the experiment.
        """
        pass
    
    


