'''

@author: eh14
'''
from twisted.trial import unittest
import pythonioc
from twutils import timeprovider
import sys
print sys.path
from cb import config
from cb.node import frequency, experiment


class BaseTest(unittest.TestCase):
    
    
    def setupTestServices(self):
        pythonioc.registerService(timeprovider.TimeProvider)
        pythonioc.registerService(config.Config)
        pythonioc.registerService(frequency.FrequencyService)
        pythonioc.registerService(experiment.ExperimentService)
