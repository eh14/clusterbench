'''
Created on Jun 19, 2014

@author: eh14
'''


import os
import sys
import pythonioc
sys.path.append(os.getcwd())

from cb import config
import zmq
from twisted.python import log
from _random import Random
import time


rand = Random(time.time())

class Node(object):
    name = None
    status = 1
    energy = 0
    uptime = 0
    
    @staticmethod
    def create(**kwargs):
        node = Node()
        for (name, value) in kwargs.iteritems():
            setattr(node, name, value)
            
        return node
    
    def toJson(self):
        # simulate no energy, if its really off.
        if self.status:
            self.energy = float(self.energy) + rand.random()
            self.uptime = int(float(self.uptime)) + 1
            self.current = str(rand.random() * 50 + 200) 
        else:
            self.energy = 0
            self.uptime = 0
            self.current = 0
            
        return {'current':str(self.current),
                'status':str(self.status),
                'energy':str(self.energy),
                'uptime':str(self.uptime)
                }

class CambrionixMock(object):
    def __init__(self, nodeNames=[]):
        if len(nodeNames) == 0:
            nodeNames = ['nodeA']
        
        self.nodeState = {name : Node.create(name='nodeA',
                      status='1',
                      energy='100',
                      uptime='123') for name in nodeNames}
    
        self.nodeState['switch-S'] = Node.create(name='switch-S',
                                        status='1',
                                        energy="50",
                                        uptime='123')
        
    def getNodeStates(self):
        return {
                'code':'nodes-state',
                'data':{n:v.toJson() for n, v in self.nodeState.iteritems()}
                }
        
    def nodeDeactivate(self, node):
        return self._changeNodeState(node, 0)
        
    def nodeActivate(self, node):
        return self._changeNodeState(node, 1)
        
    def _changeNodeState(self, node, status):
        assert status in [0, 1]
        if node not in self.nodeState:
            return {'code':'error', 'data':'Node %s was not found' % node}
        else:
            self.nodeState[node].status = status
            return {'code':'ok'}
        
    def getNetworkTopology(self):
        return {'code':'network-topology',
                'data':{'switch-S':filter(lambda x: not x.startswith('switch'), self.nodeState.keys())}}
        
    def deactivateAllNodes(self):
        """
        mainly for unit-testing....
        """
        for node in self.nodeState.itervalues():
            node.status = 0

def runCambrionixMock(context, *nodeNames):
    sock = context.socket(zmq.REP)  # @UndefinedVariable
    
    cfg = pythonioc.Inject(config.Config)
    
    sock.bind(cfg.cambrionixUrl)
    
    cambrionixmock = CambrionixMock(nodeNames)
    
    
    try:
        log.msg("Cambrionix mock bound at %s" % cfg.cambrionixUrl)
        while True:
            received = sock.recv_json()
            log.msg("Cambrionix mock received %s" % received)
            code = received['code']
            if code == 'exit':
                sock.send_json({'code':'ok'})
                break
            elif code == 'nodes-state':
                sock.send_json(cambrionixmock.getNodeStates())
            elif code == 'node-deactivate':
                sock.send_json(cambrionixmock.nodeDeactivate(received['data']))
                    
            elif code == 'node-activate':
                sock.send_json(cambrionixmock.nodeActivate(received['data']))
            elif code == 'enable-profiles':
                sock.send_json({'code':'ok'})
            elif code == 'network-topology':
                sock.send_json(cambrionixmock.getNetworkTopology())
            else:
                log.msg("mock received invalid code, will respond with error")
                sock.send_json({'code':'error'})
    finally:
        sock.close()
        
if __name__ == '__main__':
    pythonioc.registerService(config.Config)
    log.startLogging(sys.stdout)
    
    slaveNames = sys.argv[1:]
    runCambrionixMock(zmq.Context(), *slaveNames)
