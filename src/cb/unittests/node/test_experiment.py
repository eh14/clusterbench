'''

@author: eh14
'''


from cb.unittests import base
import pythonioc
import os
import shutil
from twisted.internet import defer

class TestExperimentService(base.BaseTest):
    config = pythonioc.Inject('config')
    experimentService = pythonioc.Inject('experimentService')    
    def setUp(self):
        self.setupTestServices()
        
        self.config.workingDirectory = '/tmp/test_experiment'
        
    def tearDown(self):
        if os.path.exists(self.config.workingDirectory):
            shutil.rmtree(self.config.workingDirectory)
        
    @defer.inlineCallbacks
    def test_prepareEnvironment(self):
        yield self.experimentService.prepareEnvironment()
        self.assertTrue(os.path.exists(self.config.workingDirectory))
        