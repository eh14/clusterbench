'''

@author: eh14
'''
from cb.unittests import base
from twisted.internet import defer
from cb.node import frequency

class TestFrequency(base.BaseTest):
    
    def setUp(self):
        self.setupTestServices()
        
    @defer.inlineCallbacks
    def test_getFrequency(self):
        s = frequency.FrequencyService()
        r = yield s.readFrequency()
        
        # it should have found at least one
        self.assertTrue(len(r) > 0)
        self.assertTrue(r[0] > 100000)
        
        

    @defer.inlineCallbacks
    def test_readFrequencies(self):
        f = frequency.FrequencyService()
        
        self.assertIsNone(f._frequencies)

        yield f._readFrequencies()
        self.assertTrue(len(f._frequencies) > 1)
        
        
        # now do it automatically
        f = frequency.FrequencyService()
        freqs = yield f.getFrequencies()
        self.assertTrue(len(freqs) > 0)
        
