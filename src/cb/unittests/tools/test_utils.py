'''

@author: eh14
'''
from cb.unittests import base
from twisted.internet import defer
from cb.tools import utils


class Mock(utils.CommandLineCallerMixin):
    pass

class TestCommandLineCallerMixin(base.BaseTest):
    
    def setUp(self):    
        self.m = Mock()
        
    @defer.inlineCallbacks
    def test_getProgramOutput_timeout(self):
        try:
            yield self.m._getProgramOutput(['sleep', '5'], timeout=0.1)
            self.fail('expected exception')
        except utils.CommandLineTimeout:
            pass
        