'''

@author: eh14
'''
from cb.unittests import base
from cb.tools import recorder
import pythonioc
import os
from twisted.internet import defer, task, reactor

class TestExperimentService(base.BaseTest):

    config = pythonioc.Inject('config')

    def setUp(self):    
        self.setupTestServices()
        
    @defer.inlineCallbacks
    def test_startStop(self):
        # set the experiment folder
        self.config.workingDirectory = '/tmp/'
        
        recorderTool = recorder.RecorderTool()
        
        yield recorderTool.start()
        
        # wait two seconds
        yield task.deferLater(reactor, 1.5, lambda:None)
        
        yield recorderTool.stop()

        stats = os.stat(recorderTool.getOutputFile())
        # check file exists and contains some data
        self.assertTrue(stats.st_size > 0)

        
    @defer.inlineCallbacks
    def test_doublestop(self):
        recorderTool = recorder.RecorderTool()
        yield recorderTool.stop()
        
    @defer.inlineCallbacks
    def test_getStatus(self):
        recorderTool = recorder.RecorderTool()
        status = yield recorderTool.getStatus()
        self.assertEquals(recorderTool.STATUS_STOPPED, status) 
        yield recorderTool.start()
        
        status = yield recorderTool.getStatus()
        self.assertEquals(recorderTool.STATUS_RUNNING, status) 
        
        yield recorderTool.stop()
        status = yield recorderTool.getStatus()
        self.assertEquals(recorderTool.STATUS_STOPPED, status)
        