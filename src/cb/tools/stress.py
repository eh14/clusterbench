'''

@author: eh14
'''
import subprocess
from cb.tools import BaseTool
import os
import signal
import exceptions
from twisted.python import log

class Stress(BaseTool):
    proc = None
    ERROR = object()
    
    def start(self):
        try:
            self.proc = subprocess.Popen(['stress', '-c', '1'], preexec_fn=os.setsid)
        except exceptions.OSError:
            self.proc = self.ERROR
    
    
    def getStatus(self):
        if self.proc and self.proc.poll() is None:
            return self.STATUS_RUNNING
        
        elif self.proc and (self.proc == self.ERROR or self.proc.poll() != 0):
            return self.STATUS_FAILED
        else:
            return self.STATUS_STOPPED 
    
    def stop(self):
        log.msg("Stopping Stress")
        if self.proc and self.proc != self.ERROR:
            log.msg('Attempting to kill process %d' % self.proc.pid)
            os.killpg(self.proc.pid, signal.SIGTERM)
            self.proc = None
        else:
            log.msg('Not running')
            
