'''

@author: eh14
'''

import pythonioc
import subprocess
from twisted.internet import defer, threads
from twisted.python import log

from cb import tools
from cb.tools import looper
import itertools


try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')

class PowerRecorder(tools.BaseTool):
    cambrionix = pythonioc.Inject('cambrionixService')
    name = 'Power Recorder'
    reactor = pythonioc.Inject('reactor')
    timeProvider = pythonioc.Inject('timeProvider')
    topologyService = pythonioc.Inject('topologyService')
    
    def postInit(self):
        self._recorder = None
        self._output = None
        
    def start(self, outputFile):
        
        self._output = open(outputFile, 'w')
        self._recorder = looper.createLoopingCall(self.reactor, self._record)
        
        self._nodeNames = self.topologyService.getNodeNameList()
        self._nodeNames.sort()
        
        # print the headers in our output file like this:
        # for each name in the list name1, name2, name3 etc. we'll create two columns:
        # name1-on name1-power name2-on name2-power ....
        print >> self._output, 'time', ' '.join(itertools.chain(*zip(['%s-on' % n for n in self._nodeNames],
                                            ['%s-power' % n for n in self._nodeNames])))
        
        # start the looper
        self._recorder.start(1, False)
        
    def stop(self):
        if self._recorder:
            self._recorder.stop()
            self._recorder = None
        else:
            log.msg('Recorder not running.')
        
        if not self._output:
            log.msg('WARN: stopping while no output is being open.')
        else:
            self._output.close()

    @defer.inlineCallbacks
    def _record(self):
        assert self._output
        
        values = yield self.cambrionix.readCambrionixValues()
        if not values:
            log.msg('WARNING: Reading the values failed. May be not initialized. Ignoring this round')
            return
        now = self.timeProvider.now()

        # collect values to be printed
        recordValues = []
        for name in self._nodeNames:
            recordValues.append(str(values[name]['status']))
            recordValues.append(str(values[name]['current']))
            
        print >> self._output, now, ' '.join(recordValues)
        self._output.flush()
        
        
    def getStatus(self):
        if self._recorder and self._recorder.running:
            return self.STATUS_RUNNING
        else:
            return self.STATUS_STOPPED
        
        # since the looper is fail-safe, it cannot really failF
