'''

@author: eh14
'''

import pythonioc
import subprocess
from twisted.internet import defer, threads
from twisted.python import log

from cb import tools


try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')

class RecorderTool(tools.BaseTool):
    
    config = pythonioc.Inject('config')
    name = 'recorder'
    outputFile = 'records'
    recordProcess = None
    
    ERROR = object()
    
    @defer.inlineCallbacks
    def start(self):
        outFile = self.getOutputFile()
        
        log.msg('Start Recording to file %s' % outFile)
        
        if os.path.exists(outFile):
            yield threads.deferToThread(os.remove, outFile)
        try:
            self.recordProcess = subprocess.Popen(['dstat', '--noheaders', '--noupdate', '--nocolor',
                          '-Tmn', '--cpufreq', '--output', outFile],
                                              stdout=DEVNULL,
                                              stderr=subprocess.STDOUT)
        except OSError:
            self.recordProcess = self.ERROR 
    @defer.inlineCallbacks
    def stop(self):
        yield defer.succeed(None)
        
        # still running
        if self.recordProcess not in (self.ERROR, None) and self.recordProcess.poll() is None:
            yield threads.deferToThread(self.recordProcess.terminate)
            self.recordProcess = None
        else:
            log.msg('Recorder not running')
            
    
    @defer.inlineCallbacks
    def getStatus(self):
        yield defer.succeed(None)
        
        # failed, log that
        if self.recordProcess == self.ERROR:
            defer.returnValue(self.STATUS_FAILED)
        
        # not running
        if self.recordProcess is None or self.recordProcess.poll() == 0:
            defer.returnValue(self.STATUS_STOPPED)
        
        # poll for return code and return status depending on that.
        retCode = yield threads.deferToThread(self.recordProcess.poll)
        
        if retCode is None:
            defer.returnValue(self.STATUS_RUNNING)
        
        if retCode != 0:
            defer.returnValue(self.STATUS_FAILED)
        
        raise RuntimeError('invalid service state')
