'''

@author: eh14
'''

from twisted.internet import defer, reactor
from twisted.python import log
from cb.tools import utils, BaseTool
import pythonioc
import re
from twisted.spread import pb
import twutils
from cb.node import admintool

class FrequencyService(utils.CommandLineCallerMixin):
    
    config = pythonioc.Inject('config')
    
    #
    # holds the number of CPUs.
    # Don't access this attribute directly,
    # rather use the getCpus function that lazily reads the cpus
    # and creates a list.
    _numCpus = None
    
    #
    # holds a list of frequencies.
    # Don't access that attribute directly, rather use the 
    # getter getFrequencies that lazily initializes the variable.    
    _frequencies = None
    FREQ_PATTERN = re.compile('([0-9]+):[0-9]+[, ]', re.MULTILINE)
    
    _admin = None
    
    @defer.inlineCallbacks
    def getCpus(self):
        if self._numCpus is None:
            yield self._readCpus()
        
        defer.returnValue(range(self._numCpus))
        
    @defer.inlineCallbacks
    def _readCpus(self):
        
        log.msg('Reading processor CPUs')
        output = yield self._getProgramOutput(['lscpu', '--parse=CPU'])
        
        self._numCpus = len([l for l in output.splitlines() if not 
                            l.strip().startswith('#')])
        
        log.msg('found %d cpus' % self._numCpus)

    @defer.inlineCallbacks
    def getFrequencies(self):
        if self._frequencies is None:
            yield self._readFrequencies()    
        
        defer.returnValue(self._frequencies)    
        
    @defer.inlineCallbacks
    def _readFrequencies(self):
        
        output = yield self._getProgramOutput(['cpufreq-info', '-s'])
        
        self._frequencies = [int(m.group(1)) for m in self.FREQ_PATTERN.finditer(output)]
        self._frequencies.sort()
        if not self._frequencies:
            raise RuntimeError('Error initializing frequencies')
    
    @defer.inlineCallbacks
    def readFrequency(self):
        ds = yield self._mapToCpus(lambda cpu:self._getProgramOutput(['cpufreq-info', '-f', '-c%d' % cpu]))
        
        results = yield defer.DeferredList(ds)
        
        if not all([x[0] for x in results]):
            raise RuntimeError('At least one command failed')
        
        defer.returnValue([value[1] for value in results])    
    
    @defer.inlineCallbacks
    def _getOrCreateAdmin(self):
        
        if self._admin is None:
            self._admin = yield admintool.connectToAdmin()
            
            # reset to None if connection is lost.
            self._admin.notifyOnDisconnect(lambda _:setattr(self, '_admin', None))
            
        assert self._admin
        defer.returnValue(self._admin)
        
    @defer.inlineCallbacks
    def setFrequency(self, frequency):
        admin = yield self._getOrCreateAdmin()
        ds = yield self._mapToCpus(lambda cpu:admin.callRemote('cpufreqSet', [ '-c%d' % cpu, '-f', str(frequency)]))
        yield defer.DeferredList(ds)
        
        
    @defer.inlineCallbacks
    def setOndemand(self):
        admin = yield self._getOrCreateAdmin()
        ds = yield self._mapToCpus(lambda cpu:admin.callRemote('cpufreqSet', [ '-c%d' % cpu, '-g', 'ondemand']))
        yield defer.DeferredList(ds)
    
    @defer.inlineCallbacks
    def setManual(self):
        admin = yield self._getOrCreateAdmin()
        ds = yield self._mapToCpus(lambda cpu:admin.callRemote('cpufreqSet', [ '-c%d' % cpu, '-g', 'userspace']))
        yield defer.DeferredList(ds)
        
    
    @defer.inlineCallbacks
    def _mapToCpus(self, command):
        """
        attempts to read the cpus if not present,
        runs the command for all CPUs.
        @param command: callable that takes an argument (the command)
        @returns a list of the return values of the lambdas
        """
        cpus = yield self.getCpus()
        defer.returnValue([command(cpu) for cpu in cpus])
        
        
        
class FrequencyTool(BaseTool):
    """
    This tool can watch and log cpu frequency and also
    modify it.
    
    Everything is managed via start and the appropriate command argument.
    """
    outputFile = 'cpufrequency'
    frequencyService = pythonioc.Inject(FrequencyService)
    timeProvider = pythonioc.Inject('timeProvider')
    _currentFreqIndex = None
    
    __recordFile = None
    
    def __init__(self):
        self._recorderLoop = twutils.createLoopingCall(reactor, self.write_record)
    
    @defer.inlineCallbacks
    def write_record(self):
        if not self.__recordFile or self.__recordFile.closed:
            log.msg('Warning: record file closed. Maybe race condition. Maybe error')
        
        now = self.timeProvider.now()
        values = yield self.frequencyService.readFrequency()
        after = self.timeProvider.now()
        print >> self.__recordFile, now, ' '.join(values)
        self.__recordFile.flush()
    
    @defer.inlineCallbacks
    def _startRecorder(self):
        # stop it first
        if self.__recordFile or self._recorderLoop.running:
            self.stop()
        cpus = yield self.frequencyService.getCpus()
        self.__recordFile = open(self.getOutputFile(), 'w')
        # write header
        print >> self.__recordFile, 'time', (' '.join(['cpu%s' % c for c in cpus]))
        
        # start the looper
        self._recorderLoop.start(1, False, jitter=0, minLogTime=1000)
    
    def stop(self):
        """
        Stops the recorder, if running.
        """
        self._recorderLoop.stop()
        if self.__recordFile:
            self.__recordFile.close()
            self.__recordFile = None
        
        
    @defer.inlineCallbacks
    def _setMinFrequency(self):
        # read the freqs from the service. 
        frequencies = yield self.frequencyService.getFrequencies()
        if not frequencies:
            raise RuntimeError('Frequencies seem to not have been initialized. Cannot set Frequency')
        self.frequencyService.setFrequency(frequencies[0])
        self._currentFreqIndex = 0

    @defer.inlineCallbacks
    def _setNextFrequency(self):
        
        if self._currentFreqIndex is None:
            yield self._setMinFrequency()
            return
        
        # read the freqs from the service. 
        frequencies = yield self.frequencyService.getFrequencies()
        
        # already at highest frequency
        if self._currentFreqIndex == len(frequencies) - 1:
            yield defer.succeed(None)
            return
        
        # increment frequency and assign as current index if everything went right.
        nextFreq = self._currentFreqIndex + 1
        assert nextFreq < len(frequencies)
        yield self.frequencyService.setFrequency(frequencies[nextFreq])
        self._currentFreqIndex = nextFreq
        
    @defer.inlineCallbacks
    def start(self, command='recorder'):
        """
        @param command: which command to execute with this "start"-call.
            Can be one of:
            * recorder: starts the recorder (default)
            * reset: sets the frequency to the cpu's minimum frequency.
            * next: sets frequency to the next or resets to minimum if 
                nothing is set yet. When maximum is reached, this does nothing.
            * ondemand: resets the governor back to ondemand
        """
        if command == 'recorder':
            yield self._startRecorder()
        elif command == 'reset':
            yield self._setMinFrequency()
        elif command == 'next':
            yield self._setNextFrequency()
        elif command == 'ondemand':
            yield self.frequencyService.setOndemand()
        elif command == 'manual':
            yield self.frequencyService.setManual()
        else:
            raise AttributeError('Invalid command %s specified.' % command)
    
    
    def getStatus(self):
        return self.STATUS_RUNNING if self._recorderLoop.running else self.STATUS_STOPPED 

