from twisted.internet import task, defer
from twisted.python import log
import time
import traceback
import sys
import pythonioc

reactor = pythonioc.Inject('reactor')

class _SafeFunctionRunner(object):
    def __init__(self, function, *args, **kwargs):
        self.func = function
        self.args = args
        self.kwargs = kwargs
        
        # backref to the looping call in case
        # we want to stop it from the inside after n trials.
        self.loopingCall = None
        self.maxfails = None
        self.fails = 0
        
        # time in milliseconds the task
        # needs to be running before
        # the looper prints its time
        self._minLogTime = 50
    
    @defer.inlineCallbacks
    def __call__(self):
        try:
            start = time.time()
            result = yield self.func(*self.args, **self.kwargs)
            self.fails = 0
            defer.returnValue(result)
        except Exception as e:
            _exc_type, _exc_value, exc_traceback = sys.exc_info()
            tb = traceback.extract_tb(exc_traceback)
            exc_location = traceback.format_list(tb[-3:])
            log.msg('Exception occured running LoopingCall %s: %s\n(%s)' % (self.name, str(e), exc_location))
            self.fails += 1
        finally:
            looperTime = round((time.time() - start) * 1000, 2)
            
            if looperTime > self._minLogTime:
                log.msg('Looping call %s took %f ms' % (self.name, looperTime))
                
            if self.maxfails and self.fails >= self.maxfails:
                log.msg("Looper failed too many times (max %s). I'm giving up" % self.maxfails)
                self.loopingCall.stop()
            

    def setMinLogTime(self, minLogTime):
        self._minLogTime = minLogTime
        
    @property
    def __name__(self):
        return self.func.__name__
    
    @property
    def name(self):
        return self.func.__name__
    
    def setMaxFails(self, loopingCall, maxfails):
        """
        Specify max number of fails along with the job that needs to be 
        stopped when the max fails are reached.
        """
        assert loopingCall, "Need job to stop when max fails reached"
        assert maxfails > 0, "maxfails needs to be > 0"
        self.maxfails = maxfails
        self.loopingCall = loopingCall
        
    def __str__(self):
        return 'Function Runner: %s *%s **%s' % (self.func, self.args, self.kwargs)
        
    def __repr__(self):
        return str(self)
    
def createLoopingCall(reactor, function, *args, **kwargs):
    looper = task.LoopingCall(_SafeFunctionRunner(function, *args, **kwargs))
    # replace the reactor with out injected reactor
    looper.clock = reactor
    
    return looper

class _OneShotFunctionRunner(_SafeFunctionRunner):
    
    @defer.inlineCallbacks        
    def __call__(self):
        yield _SafeFunctionRunner.__call__(self)
        if self.fails == 0 and self.loopingCall.running:
            self.loopingCall.stop()

def startRetryingOneshotJob(reactor, function, args=(), kwargs={}, maxRetries=32, tryInterval=3.2, now=False):
    """
    Creates a oneshot job that executes once and tries maxRetries in case
    it crashes. After first successful execution it is being stopped.
    
    Best to use named parameters only!
    """
    runner = _OneShotFunctionRunner(function, *args, **kwargs)
    looper = task.LoopingCall(runner)
    looper.clock = reactor
    looper.start(interval=tryInterval, now=now)
    runner.setMaxFails(looper, maxRetries)
    
    return looper

