'''

@author: eh14
'''
import subprocess
from twisted.internet import defer, reactor, threads, task
import pythonioc
from twisted.python import threadable
from functools import wraps


class CommandLineException(Exception):
        pass
    
class CommandLineTimeout(Exception):
    pass

class CommandLineCallerMixin(object):
    
    timeProvider = pythonioc.Inject('timeProvider')

    @defer.inlineCallbacks    
    def _getProgramOutput(self, cmdLine, timeout=2):
        p = subprocess.Popen(cmdLine, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        end = self.timeProvider.now() + timeout
        while self.timeProvider.now() < end:
            # defer execution
            
            # poll result
            retCode = p.poll()
            # not finished 
            if retCode is None:
                yield task.deferLater(reactor, 0.1, lambda: None)
                continue
            if retCode != 0:
                raise CommandLineException('Program failed with code %s' % retCode)
             
            assert retCode == 0

            # get stdout and return             
            (stdout, _) = p.communicate()
            defer.returnValue(stdout.strip())
            
        else:
            raise CommandLineTimeout('Program did not terminate within timeout %d' % timeout)
            


    @defer.inlineCallbacks
    def _catSystemFile(self, fileName, readLines=False):
        def _read():
            with open(fileName, 'r') as gov:
                if readLines:
                    return gov.readLines()
                else:
                    return gov.read().strip()
            
        content = yield threads.deferToThread(_read)
        defer.returnValue(content)


def isReactorThread():
    return threadable.isInIOThread()

def runAsDeferredThread(func):
    """
    Decorates a function to run as a deferred thread when invoked in the reactor thread
    or reuse the same thread when invoked in a deferred thread already.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if isReactorThread():
            # print "creating new thread"
            return threads.deferToThread(func, *args, **kwargs)
        else:
            # print "run it directly"
            result = func(*args, **kwargs)
            assert not isinstance(result, defer.Deferred), "The inner function must be a deferred!"
            return result
    return wrapper