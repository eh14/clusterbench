"""
Simple Pb-Objects for file Transfer.
"""
from twisted.internet import defer, task, reactor
from twisted.spread import pb
import os


@defer.inlineCallbacks
def writeFile(sourceFile, target, chunkSize, delay):
    if not os.path.exists(sourceFile):
        raise RuntimeError('File does not exist')
    
    with open(sourceFile, 'r') as source:
        try:
            target.callRemote('newFile', os.path.basename(sourceFile))
            
            while True:
                chunk = source.read(chunkSize)
                yield target.callRemote('chunk', chunk)
                
                if not chunk:
                    break
                
                # wait for "delay"
                # yield task.deferLater(reactor, delay, lambda:None)
        finally:
            # in any case, close the remote file or we corrupt file transfer
            yield target.callRemote('done')
        

class TargetFile(pb.Referenceable):
    
    def __init__(self, targetFolder, output):
        self.f = None
        self._targetFolder = targetFolder
        self._output = output
        
        if not os.path.exists(targetFolder):
            os.makedirs(targetFolder)
        
    def remote_newFile(self, name):
        assert self.f is None, 'trying to write two files simultaneously'
        self.f = open(os.path.join(self._targetFolder, name), 'wb')
        self._output.send('Open file for writing: ' + name)
        
    def remote_chunk(self, data):
        self.f.write(data)
        
    def remote_done(self):
        
        self.f.close()
        self.f = None

        self._output.send('...done')
        
