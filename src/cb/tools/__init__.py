import pythonioc
import os


class BaseTool(object):
    
    STATUS_STOPPED = 'stopped'
    STATUS_RUNNING = 'running'
    STATUS_FAILED = 'failed'
    
    config = pythonioc.Inject('config')
    def start(self, *args, **kwargs):
        raise NotImplementedError('abstract method')
    
    
    def stop(self):
        raise NotImplementedError('abstract method')
    
    def getStatus(self):
        raise NotImplementedError('abstract method')
    
    @classmethod
    def getName(cls):
        if hasattr(cls, 'name'):
            return cls.name
        else:
            return cls.__name__


    def getOutputFile(self):        
        if hasattr(self, 'outputFile'):
            outputFileName = self.outputFile
        else:
            outputFileName = self.getName()
        return os.path.join(self.config.workingDirectory, outputFileName)
    
