'''

@author: eh14
'''

from twisted.internet import defer, reactor
from twisted.python import log
from cb.tools import utils, BaseTool
import pythonioc
import re
from twisted.spread import pb
import twutils
from cb.node import admintool

        
class TemperatureTool(BaseTool, utils.CommandLineCallerMixin):
    """
    This tool can watch and log cpu frequency and also
    modify it.
    
    Everything is managed via start and the appropriate command argument.
    """
    outputFile = 'temperature'
    timeProvider = pythonioc.Inject('timeProvider')
    
    name = 'TemperatureTool'
    
    __recordFile = None
    
    def __init__(self):
        self._recorderLoop = twutils.createLoopingCall(reactor, self.write_record)
    
    @defer.inlineCallbacks
    def write_record(self):
        if not self.__recordFile or self.__recordFile.closed:
            log.msg('Warning: record file closed. Maybe race condition. Maybe error')
        
        time = self.timeProvider.now()
        
        try:
            temp = yield  self._catSystemFile('/sys/devices/platform/sunxi-i2c.0/i2c-0/0-0034/temp1_input')
            print >> self.__recordFile, time, temp
        except Exception as e:
            print >> self.__recordFile, time, str(e).strip()
            
        self.__recordFile.flush()
    
    @defer.inlineCallbacks
    def start(self):
        # stop it first
        if self.__recordFile or self._recorderLoop.running:
            self.stop()
        self.__recordFile = open(self.getOutputFile(), 'w')
        # write header
        print >> self.__recordFile, 'time', 'temperature'
        
        # start the looper
        self._recorderLoop.start(1, False, jitter=0, minLogTime=1000)
    
    def stop(self):
        """
        Stops the recorder, if running.
        """
        self._recorderLoop.stop()
        if self.__recordFile:
            self.__recordFile.close()
            self.__recordFile = None
            
    def getStatus(self):
        return self.STATUS_RUNNING if self._recorderLoop.running else self.STATUS_STOPPED 

