import cmd
import logging

import sys
print sys.path

from twisted.internet import reactor, threads, defer, task
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.spread import pb
from twisted.internet.error import ConnectionRefusedError
import os
from cb import experiment
from cb.server import protocol
import argparse
import imp


EXPERIMENT_CONFIG_FILE = 'experiment.cfg'

argParse = argparse.ArgumentParser()

argParse.add_argument('--working-directory', dest='workingDir',
                      help='Working Directory. Defaults to current dir', default=os.getcwd())

argParse.add_argument('--experiment', dest='experiment',
                       help='When specified, executes the experiment file instead',
                       default=None)

log = logging.getLogger('Client')

class ClientOutput(pb.Referenceable):
    def remote_out(self, what):
        print what.rstrip()

class Client(cmd.Cmd):
    
    _dead = False
    def __init__(self, server, options):
        cmd.Cmd.__init__(self)
        
        self._server = server
        self._workingDir = options.workingDir
        
        def _onDisconnect(reason):
            print "Root object disconnected. The server may have died. Exiting"
            self._dead = True
    
        self._server.notifyOnDisconnect(_onDisconnect)
        
    def emptyline(self):
        return False
    
    def postcmd(self, stop, line):
        stop = stop or self._dead
        if stop:
            reactor.callFromThread(reactor.stop)
            
            if self._dead:
                raise SystemExit(0)
            
        return stop
        
    def do_EOF(self, line):
        return self.do_exit(line)
    
    def do_exit(self, line):
        print "See you!"
        return True
    
    def _callServer(self, method, printResult=True, args=(), kwargs={}):
        """
        calls the remote method on the server's PB-Object-copy
        """
        
        @defer.inlineCallbacks
        def _doCall(method, *_a, **_kw):
            try:
                d = yield self._server.callRemote(method, *_a, **_kw)
                defer.returnValue(d)
            except Exception as e:
                print "Error executing server command", e
                defer.returnValue(e)
                
        result = threads.blockingCallFromThread(reactor, _doCall, method, *args, **kwargs)
        
        # print only if there is a result, otherwise the console will be full of "None"s
        if result is not None and printResult:
            print result
            
        return result
            
    def do_prepare_experiment(self, line):
        values = self._splitAndStrip(line)
        if not values:
            print "Need name for experiment"
            return
        
        self._callServer('prepareExperiment', args=(values[0],))
        return False
    
    def do_get_results(self, line):
        self._callServer('retrieveFiles')
        
    def do_pwd(self, line):
        """ Prints the current working diretory """
        print self._workingDir
        
        return False 
    
    def do_expwd(self, line):
        """
        Prints the current experiment working directory used by the server. Change by doing prepare_experiment
        """
        self._callServer('getCurrentExperimentFolder')
    
    def do_tool_status(self, line):
        toolStats = self._callServer('getToolStatus', printResult=False)
        
        # sort by name
        toolStats.sort(key=lambda x:x[0])
        for name, values in toolStats:
            print " ", name, ' '.join(['%s->%s' % (tool, toolStats) for tool, toolStats in values.iteritems()])
        
        return False
    
    def do_start_tool(self, line):
        self._tool_control(line, 'startTool')
        
    def do_stop_tool(self, line):
        self._tool_control(line, 'stopTool')
        
    def do_stop_all(self, line):
        self._callServer('stopAll')
        
    def do_start_power_recorder(self, line):
        self._callServer('startPowerRecorder')
        
    def do_stop_power_recorder(self, line):
        self._callServer('stopPowerRecorder')
        
    def do_clusterstate(self, line):
        """ shows the online/connected state of the whole cluster. """
        result = self._callServer('clusterState', printResult=False)
        
        result.sort(key=lambda x:x['name'])
        
        print "Cluster State"
        print "============="
        
        for node in result:
            print " {name} on {on} connected {con}".format(name=node['name'],
                                                           on=int(node['power']),
                                                                  con=int(node['online'])
                                                        ) 
        
            
    def do_turnon(self, line):
        """ Turn on nodes. Arguments are the names of the nodes
        """
        nodes = self._splitAndStrip(line)
        if not nodes:
            print "Provide at least one node name"
            return
        
        self._callServer('turnOnNodes', args=nodes)
        
    def do_shutdown(self, line):
        """ Turn off nodes. Arguments are the names of the nodes
        """
        nodes = self._splitAndStrip(line)
        if not nodes:
            print "Provide at least one node name"
            return
        
        self._callServer('turnOffNodes', args=nodes)
        
    def do_do(self, line):
        """ Executes a command on all nodes and prints the results synchronously.
        """
        result = self._callServer('executeCommand', args=(line,), printResult=False)
        
        result.sort(lambda a, b: cmp(a[0], b[0]))
        for res in result:
            print '-----------', res[0], '-------------', '\n', res[1]
        
    def _tool_control(self, line, action):
        line = line.strip()
        args = self._splitAndStrip(line)
        if not args:
            print "Need at least tool name as argument!"
            return False
        
        self._callServer(action, args=tuple(args))
        
    
    def do_list_tools(self, line):
        tools = self._callServer('getToolList', printResult=False)
        
        print "Tools:"
        for tool in tools:
            print '  * ' + tool
    
    def _splitAndStrip(self, value, sep=' '):
        return [a for a in value.split(sep) if a]
    
    
    def do_collect_offsets(self, line):
        result = self._callServer('collectTimeOffset', printResult=False)
        
        print "Node Offsets"
        for name, offset in result.iteritems():
            print "  %s: %.6f seconds" % (name, offset)
        
    
@defer.inlineCallbacks
def getAndPrepareRootObject(factory):
    root = yield factory.getRootObject()
    # register a referenceable that the server can use
    # to do remote printing for async results
    yield root.callRemote('setOutput', ClientOutput())
    yield root.callRemote('connect')
    
    defer.returnValue(root)


@defer.inlineCallbacks
def runCommandClient(factory, args):    
    
    root = yield getAndPrepareRootObject(factory)
    
    cmd = Client(root, args)    
    threads.deferToThread(cmd.cmdloop)
    
def _exit(msg):
    print msg
    reactor.stop()
    
@defer.inlineCallbacks
def runExperimentClass(factory, args):
    # yield something if errors happen and nothing is yielded otherwise
    yield defer.succeed(None)
    
    
    expFile = os.path.join(args.workingDir, args.experiment)
    if not os.path.exists(expFile):
        _exit('Experiment file %s does not exist' % expFile)
        return
    
    moduleName = os.path.splitext(args.experiment)[0]
    
    with open(expFile, 'r') as modSrc:
        mod = imp.load_source(moduleName, os.path.dirname(expFile), modSrc)
    if not hasattr(mod, 'experiment'):
        _exit('Failed to load experiment. Define a global variable "experiment" and assign the Experiment instance')
        return
    
    if not isinstance(mod.experiment, experiment.Experiment):
        _exit('Experiment needs to be instance of experiment.Experiment')
        return
    
    root = yield getAndPrepareRootObject(factory)
    mod.experiment.server = root
    
    yield mod.experiment.prepare()
    
    yield mod.experiment.run()
    
    factory.disconnect()
    _exit('Experiment Done')
    
        
@defer.inlineCallbacks   
def init(connection, factory, args):
    # wait for connection
    try:
        yield connection
    except ConnectionRefusedError as e:
        print "Error connecting to the server"
        print e
        print "Exiting"
        reactor.stop()
    
    if args.experiment:
        yield task.deferLater(reactor, 5, lambda:None)
        yield runExperimentClass(factory, args)
    else:
        yield runCommandClient(factory, args)
if __name__ == '__main__':
    args = argParse.parse_args()
    
    factory = pb.PBClientFactory()
    endpoint = TCP4ClientEndpoint(reactor, 'localhost', 8801, timeout=2)
    connection = endpoint.connect(factory)
    reactor.callLater(0, init, connection, factory, args)
    reactor.run()
