import os, sys, pythonioc
import socket
sys.path.append(os.path.abspath('src'))

from cb import node

config = pythonioc.Inject('config')
config.name = socket.gethostname()
config.serverHost='192.168.0.201'

config.workingDirectory='/tmp/clusterbench/node/'
application = node.createApplication()

